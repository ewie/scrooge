from flask import (
    Blueprint,
    abort, redirect, render_template, url_for,
)
from flask_wtf import FlaskForm
from sqlalchemy.exc import IntegrityError
from wtforms import StringField
from wtforms.validators import DataRequired

from ..model import (
    Account,
    db, key_violation,
)


bp = Blueprint('account', __name__, url_prefix='/account')


@bp.get('')
def index():
    with db.session.begin():
        accounts = db.session.execute(
            db.select(Account).order_by(Account.name)
        ).scalars()
    return render_template('account/index.html', accounts=accounts)


@bp.get('/new')
def new():
    return render_template('account/new.html', account=AccountForm())


@bp.post('')
def create():
    form = AccountForm()

    if not form.validate():
        abort(400)

    try:
        with db.session.begin():
            db.session.add(form.make_account())
    except IntegrityError as exc:
        if key_violation(exc, 'account.name'):
            abort(400)
        raise

    return redirect(url_for('account.index'))


class AccountForm(FlaskForm):
    name = StringField(
        validators=[DataRequired()],
    )

    def make_account(self):
        return Account(name=self.name.data)
