from flask import redirect, url_for

from . import account


def init_app(app):
    app.register_blueprint(account.bp)

    @app.get('/')
    def index():
        return redirect(url_for('account.index'))
