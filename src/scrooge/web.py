from flask import Flask

from . import model, view


def make_app(config=None):
    app = Flask(__name__)

    if config:
        app.config.from_object(config)
    else:
        app.config.from_envvar('FLASK_CONFIG')

    model.init_app(app)
    view.init_app(app)

    return app
