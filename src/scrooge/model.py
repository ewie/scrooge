from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


def init_app(app):
    db.init_app(app)

    with app.app_context():
        db.create_all()


def key_violation(exc, colname):
    """Check if an exception identifies a column's unique constraint violation.
    """
    return exc.orig.args[0] == f"UNIQUE constraint failed: {colname}"


class Account(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False, unique=True)
