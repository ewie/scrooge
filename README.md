# Scrooge

Scrooge is a website to track your personal finances.


## Development

Install scrooge in a virtual environment:

    python -mvenv .venv
    source .venv/bin/activate
    pip install -e .[dev]

Lint source code and run tests:

    flake8
    pytest

Run Flask application on http://localhost:5000:

    cp config.sample.py config.py
    export FLASK_CONFIG="$PWD/config.py"
    ./flask run

Environment variable `FLASK_CONFIG` defaults to `$PWD/config.py` if not
specified as above.
