import secrets

SECRET_KEY = secrets.token_bytes()
SQLALCHEMY_DATABASE_URI = 'sqlite://'
