import pytest

from scrooge import web


@pytest.fixture
def app():
    from . import config
    yield web.make_app(config)
