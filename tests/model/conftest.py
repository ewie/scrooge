from contextlib import contextmanager

import pytest
from sqlalchemy.exc import IntegrityError

from scrooge import model


@pytest.fixture
def db(app):
    with app.app_context():
        yield model.db


@pytest.fixture
def validate(db):
    yield Validate(db)


class Validate:
    def __init__(self, db):
        self._db = db

    def not_null(self, colname):
        return self._raises(f"NOT NULL constraint failed: {colname}")

    def unique(self, colname):
        return self._raises(f"UNIQUE constraint failed: {colname}")

    @contextmanager
    def _raises(self, msg):
        with (
            pytest.raises(IntegrityError) as exc,
            self._db.session.begin(),
        ):
            yield

        assert exc.value.orig.args[0] == msg
