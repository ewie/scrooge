from scrooge.model import Account


def test_nonnull_name(db, validate):
    with validate.not_null('account.name'):
        db.session.add(Account())


def test_unique_name(db, validate):
    with validate.unique('account.name'):
        db.session.add(Account(name='foo'))
        db.session.add(Account(name='foo'))
