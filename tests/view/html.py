import bs4


class Document:
    def __init__(self, source):
        self._doc = bs4.BeautifulSoup(source, features='html.parser')

    @property
    def csrf_token(self):
        for e in self._doc.select('input[name="csrf_token"]'):
            return e['value']

    def table(self):
        for tab in self._doc.select('table'):
            return [
                tuple(c.string for c in r.find_all(['td', 'th']))
                for r in tab.find_all('tr')
            ]
