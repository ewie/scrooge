import pytest


@pytest.fixture
def client(app):
    yield app.test_client()
