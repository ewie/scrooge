from .html import Document


def test_root_redirect(client):
    res = client.get('/')
    assert res.status_code == 302
    assert res.location == '/account'


def test_zero_accounts(client):
    accounts = get_accounts(client)
    assert accounts == [('Account',)]


def test_new_account(client):
    name = 'foo'
    res = create_account(client, name)
    assert res.status_code == 302
    assert res.location == '/account'

    accounts = get_accounts(client)
    assert accounts == [
        ('Account',),
        (name,),
    ]


def test_duplicate_account(client):
    name = 'foo'
    res = create_account(client, name)
    assert res.status_code == 302
    assert res.location == '/account'

    res = create_account(client, name)
    assert res.status_code == 400

    res = client.get('/account')
    assert res.status_code == 200

    accounts = get_accounts(client)
    assert accounts == [
        ('Account',),
        (name,),
    ]


def get_accounts(client):
    res = client.get('/account')
    assert res.status_code == 200

    return Document(res.data).table()


def create_account(client, name):
    res = client.get('/account/new')
    assert res.status_code == 200

    doc = Document(res.data)
    return client.post('/account', data={
        'csrf_token': doc.csrf_token,
        'name': name,
    })
